# Easily add custom domain mappings on OS X

Tired of manually managing your `/etc/hosts` file? Wouldn't it be easier if you could add custom domain mappings from a file of your choice?

Example:

	./config-dnsmasq.sh my-domains.conf

...where `my-domains.conf` is a list of host mappings as follows:

	12.34.56.78    dev
	90.12.34.56    stage

Now domains like `myapp.dev` and `myapp.stage` will resolve to the IPs above.

## Notes

- Make sure Homebrew is installed
- It is a good idea to do `brew up` first

## Install/uninstall

	./install-dnsmasq.sh     Install dnsmasq

	./remove-dnsmasq.sh      Remove dnsmasq and its config

## Usage

	./config-dnsmasq.sh [HOSTS_CONFIG_FILE]
						     Configure dnsmasq using the specified config file. If blank, uses `hosts.conf`

	./remove-resolvers.sh [HOSTS_CONFIG_FILE]
						     Remove resolvers using the specified config file. If blank, uses `hosts.conf`
	
	./dnsmasqctl.sh          Start and stop dnsmasq

## Setup

Add your host name to IP mappings in `hosts.conf` in hosts file format, e.g.

	12.34.56.78    dev
	90.12.34.56    stage

This will cause all `.dev` domains, e.g. `myapp.dev`, to resolve to the specified IP address.

**Note:** make sure there is a line break after the last entry.

# Contributing

Pull requests welcome.

# Credit

* Made by Pete Cornish
* Uses dnsmasq for DNS magic
* Uses Homebrew for installation
* See http://passingcuriosity.com/2013/dnsmasq-dev-osx/ for inspiration.