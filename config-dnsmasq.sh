#!/bin/bash

# ------------------------------------------------------
# Setup dnsmasq on a Mac
#
# See http://passingcuriosity.com/2013/dnsmasq-dev-osx/ for inspiration.
#
# Notes:
# - Make sure you have run ./install-dnsmasq.sh first
# ------------------------------------------------------

# address of dnsmasq - don't confuse this with the domain IP
NAMESERVER_IP="127.0.0.1"

# default hosts config file
DEFAULT_HOSTS_CONFIG="./hosts.conf"

# optional parameter
HOSTS_CONFIG_FILE="$1"

function determineConfigFile() {
	if [[ "${HOSTS_CONFIG_FILE}" == "" ]]; then
		echo "No hosts config file specified - using: ${DEFAULT_HOSTS_CONFIG}"
		HOSTS_CONFIG_FILE="${DEFAULT_HOSTS_CONFIG}"
	else
		echo "Using hosts config file: ${HOSTS_CONFIG_FILE}"
	fi
}

function addBaseConfig() {
	echo "Adding sample config file"

	# Copy the default configuration file.
	cp $(brew list dnsmasq | grep /dnsmasq.conf.example$) /usr/local/etc/dnsmasq.conf
}

function addDnsmasqConfig() {
	echo "Adding mapping ${2}->${1}"

	printf "\n# Add mapping for .${2} domain\naddress=/${2}/${1}\n" >> /usr/local/etc/dnsmasq.conf
}

function addResolverConfig() {
	echo "Adding resolver for ${1}"

	printf "nameserver ${NAMESERVER_IP}\n" | sudo tee /etc/resolver/${1}
}

function addCustomConfig() {
	echo "Adding custom config"

	while IFS= read -r line
	do
		DOMAIN_ADDRESS="$( echo $line | awk '{print $1}' )"
		DOMAIN_NAME="$( echo $line | awk '{print $2}' )"

	    # add mapping
		addDnsmasqConfig ${DOMAIN_ADDRESS} ${DOMAIN_NAME}

		# add resolver
		addResolverConfig ${DOMAIN_NAME}
	done <"${HOSTS_CONFIG_FILE}"
}

function restartDnsmasq() {
	# restart after config change
	bash ./dnsmasqctl.sh stop && ./dnsmasqctl.sh start
	
	#flush cache
	dscacheutil -flushcache
}

determineConfigFile
addBaseConfig
addCustomConfig
restartDnsmasq
