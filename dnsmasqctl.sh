#!/bin/bash

# ------------------------------------------------------
# Start/stop dnsmasq on a Mac
#
# See http://passingcuriosity.com/2013/dnsmasq-dev-osx/ for inspiration.
#
# Notes:
# - Make sure you've run ./setup-dnsmasq.sh first
# ------------------------------------------------------

if [[ "$1" == "start" ]]; then
	echo "Starting dnsmasq"
	sudo launchctl start homebrew.mxcl.dnsmasq

elif [[ "$1" == "stop" ]]; then
	echo "Stopping dnsmasq"
	sudo launchctl stop homebrew.mxcl.dnsmasq

else
	echo "Please specify start or stop"
fi
