#!/bin/bash

# ------------------------------------------------------
# Install dnsmasq on a Mac
#
# See http://passingcuriosity.com/2013/dnsmasq-dev-osx/ for inspiration.
#
# Notes:
# - Make sure Homebrew is installed
# - It is a good idea to do 'brew up' first
# ------------------------------------------------------

function installDnsmasq() {
    if [[ -d "/usr/local/opt/dnsmasq" ]]; then
        echo "dnsmasq is installed"

    else
        echo "dnsmasq is not installed"
        read -p "Install it? [y/N]: " INSTALL_DNSMASQ

        if [[ "${INSTALL_DNSMASQ}" == "y" ]]; then
    		echo "Installing dnsmasq using brew"
    		brew install dnsmasq
		else
			echo "Not installing dnsmasq"
        fi
    fi
}

function configureAutostart() {
	echo "Configuring autostart"

	# Copy the daemon configuration file into place.
	sudo cp $(brew list dnsmasq | grep /homebrew.mxcl.dnsmasq.plist$) /Library/LaunchDaemons/

	# Start Dnsmasq automatically.
	sudo launchctl load /Library/LaunchDaemons/homebrew.mxcl.dnsmasq.plist
}

function addResolverDir() {
	echo "Adding resolver dir"

	sudo mkdir -p /etc/resolver
}

installDnsmasq
addResolverDir
configureAutostart
