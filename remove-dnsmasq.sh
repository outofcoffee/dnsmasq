#!/bin/bash

# ------------------------------------------------------
# Remove dnsmasq from a Mac
# ------------------------------------------------------

function removeAutostart() {
	echo "Removing autostart"

	# Don't start Dnsmasq automatically.
	sudo launchctl unload /Library/LaunchDaemons/homebrew.mxcl.dnsmasq.plist

	# Remove the daemon configuration file
	sudo rm /Library/LaunchDaemons/homebrew.mxcl.dnsmasq.plist
}

function removeResolverConfig() {
	bash ./remove-resolvers.sh
}

function removeBaseConfig() {
	echo "Removing dnsmasq config file"

	# Remove the configuration file.
	rm /usr/local/etc/dnsmasq.conf
}

function removeDnsmasq() {
    if [[ -d "/usr/local/opt/dnsmasq" ]]; then
        echo "dnsmasq is installed"

		echo "Removing dnsmasq using brew"
		brew remove dnsmasq

    else
        echo "dnsmasq is not installed"
    fi
}

removeAutostart
removeResolverConfig
removeBaseConfig
removeDnsmasq
