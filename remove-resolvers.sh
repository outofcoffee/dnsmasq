#!/bin/bash

# ------------------------------------------------------
# Remove custom resolvers
# ------------------------------------------------------

# default hosts config file
DEFAULT_HOSTS_CONFIG="./hosts.conf"

# optional parameter
HOSTS_CONFIG_FILE="$1"

function determineConfigFile() {
	if [[ "${HOSTS_CONFIG_FILE}" == "" ]]; then
		echo "No hosts config file specified - using: ${DEFAULT_HOSTS_CONFIG}"
		HOSTS_CONFIG_FILE="${DEFAULT_HOSTS_CONFIG}"
	else
		echo "Using hosts config file: ${HOSTS_CONFIG_FILE}"
	fi
}

function removeCustomConfig() {
	echo "Removing all custom config"

	while IFS= read -r line
	do
		DOMAIN_NAME="$( echo $line | awk '{print $2}' )"

		echo "Removing resolver for ${DOMAIN_NAME}"

	    sudo rm /etc/resolver/${DOMAIN_NAME}
	done <"${DEFAULT_HOSTS_CONFIG}"
}

determineConfigFile
removeCustomConfig
